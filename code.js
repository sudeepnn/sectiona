const userSlider = document.getElementById('userSlider');
const plan1 = document.getElementById('plan1');
const plan2 = document.getElementById('plan2');
const plan3 = document.getElementById('plan3');

userSlider.addEventListener('input', updateHighlightedPlan);

function updateHighlightedPlan() {
  const userCount = parseInt(userSlider.value);
  document.getElementById('userCount').textContent = userCount;
  
  
  plan1.classList.remove('highlighted');
  plan2.classList.remove('highlighted');
  plan3.classList.remove('highlighted');


  if (userCount >= 0 && userCount < 10) {
    plan1.classList.add('highlighted');
  } else if (userCount >= 10 && userCount < 20) {
    plan2.classList.add('highlighted');
  } else {
    plan3.classList.add('highlighted');
  }
}


updateHighlightedPlan();






// Get modal
const modal = document.getElementById('myModal');
const buttons = document.querySelectorAll('.open-modal-button');
const closeButton = document.querySelector('.close');


buttons.forEach((button) => {
  button.addEventListener('click', () => {
    modal.style.display = 'block';
  });
});


closeButton.addEventListener('click', () => {
  modal.style.display = 'none';
});


window.addEventListener('click', (event) => {
  if (event.target === modal) {
    modal.style.display = 'none';
  }
});

// Handle form submission
// const form = document.getElementById('modalForm');

// form.addEventListener('submit', (event) => {
  // event.preventDefault();

  // 
  // const name = document.getElementById('name').value;
  // const email = document.getElementById('email').value;
  // const comments = document.getElementById('comments').value;

  //

  // const formData = {
  //   name: name,
  //   email: email,
  //   comments: comments,
  // };

  // 
  // fetch('https://forms.maakeetoo.com/formapi/578', {
  //   method: 'POST',
  //   headers: {
  //     'Content-Type': 'application/json',
  //   },
  //   body: JSON.stringify(formData),
  // })
  //   .then((response) => {
  //     if (!response.ok) {
  //       throw new Error(`HTTP error! Status: ${response.status}`);
  //     }
  //     return response.json();
  //   })
  //   .then((data) => {
  //     
  //     console.log('Server Response:', data);
  //   })
  //   .catch((error) => {
  //    
  //     console.error('Fetch Error:', error);
  //   });



  // Close the modal
//   modal.style.display = 'none';

// 
//   form.reset();
// });
